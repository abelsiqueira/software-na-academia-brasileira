Fase 1
======

Nessa primeira fase,
obteremos experiência ao circular o questionário
entre os pesquisadores de um departamento ou instituto.

Calendário
----------

- 12/06 - Concluir questionário
- 12/06 - Primeira rodada de divulgação do questionário
- 29/06 - Segunda e última rodada de divulgação do questionário
- 03/07 - Salvar respostas dos questionários em https://gitlab.com/rgaiacs/software-na-academia-brasileira
- 07/07 - Finalizar primeira versão do artigo
- 10/07 - Submeter artigo para http://wssspe.researchcomputing.org.uk/wssspe5-1/

Documentos
----------

.. toctree::
   :maxdepth: 1

   questionario
