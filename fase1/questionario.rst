Questionário
============

Estamos realizando esse questionário
para identificar a importância de programas de computador científico
no processo de gerar, processar ou analisar resultados que serão incluídos em uma publicação.
Para isso,
gostaríamos da sua colaboração, preenchendo esse questionário.
O tempo estimado de preenchimento é 5 minutos.

Em caso de dúvidas quanto ao preenchimento favor enviar e-mail para
`incoming+rgaiacs/software-na-academia-brasileira@gitlab.com <mailto:incoming+rgaiacs/software-na-academia-brasileira@gitlab.com>`_.

Termos e condições
------------------

- Você está respondendo esse questionário voluntariamente.
- Esse questionário não pede nenhuma informação pessoal que permita identificá-lo.
- Os dados coletados serão disponibilizados em https://zenodo.org/.

1. Você concorda com os termos e condições anteriores?

   - Sim
   - Não

O que é programa de computador científico?
----------------------------

Para esse questionário, definimos "programa de computador científico" como

    Programa de computador que é utilizado para gerar, processar ou analisar
    resultados que você pretende incluir em uma publicação
    (seja em um periódico, conferência, monografia, dissertação, tese ou livro).

    Programa de computador científico pode ser
    algumas poucas linha de código escrito por você,
    um pacote desenvolvido por um profissional
    e qualquer coisa entre esses dois estremos.

    Programa de computador que não gera, processa ou analisa resultados
    (como, por exemplo, processadores de texto ou buscadores)
    não contam como "programa de computador científico" para essa pesquisa.

2. Você utiliza programa de computador científico?

   - Sim
   - Não

3. Quais os programa de computadors científicos que você utiliza com maior frequência?

Treinamento
-----------

4. Você obteve algum treinamento nos programa de computadors científicos que utiliza?

   - Sim, obtive treinamento em todos os programa de computadors científicos que utilizo.
   - Sim, obtive treinamento mais de 75% dos programa de computadors científicos que utilizo.
   - Sim, obtive treinamento mais de 50% dos programa de computadors científicos que utilizo.
   - Sim, obtive treinamento mais de 25% dos programa de computadors científicos que utilizo.
   - Sim, obtive treinamento mais de 5% dos programa de computadors científicos que utilizo.
   - Não, sou autodidata nos programa de computadors científicos que utilizo através de
     livros e material online.

5. Se vocế obtive algum treinamento,
   na maioria das vezes quem ministrou o treinamento?

   - criador do programa de computador científico
   - parceiro oficial do criador do programa de computador científico
   - empresa, instituição ou indivíduo especializado nesse tipo de treinamento
   - usuário

Desenvolvimento de Programa de Computador
-----------------------------------------

6. Você desenvolve ou desenvolveu pelo menos um dos programa de computador científicos que utiliza?

   Nessa questão estamos considerando qualquer programa de computador científico,
   de um pequeno script utilizado apenas por você
   até um complexo pacote computacional.

   - Sim
   - Não

7. Você recebeu algum treinamento em metodologia para o desenvolvimento de programa de computador?

   - Sim, participei de um curso, presencial ou online, ou workshop sobre o tema
   - Não, sou autodidata no desenvolvimento dos meus programa de computadors científicos

Pesquisa
--------

8. O que aconteceria se você não mais pudesse utilizar um dos programa de computadors
   científicos que utiliza hoje?

   - não faria diferença na minha pesquisa
   - minha pesquisa precisaria de mais horas de trabalho, mas ainda seria
     possível realizá-la
   - não seria prático conduzir minha pesquisa sem os programa de computadors dos quais
     disponho hoje

9. Você já incluiu o custo do programa de computador científico que utiliza em um projeto de
    pesquisa?

    - Sim, para compra e desenvolvimento do programa de computador científico
    - Sim, para compra do programa de computador científico
    - Sim, para o desenvolvimento do programa de computador científico
    - Não

Demografia
----------

10. Em qual estágio de carreira você se enquadra?

   - aluno/aluna de graduação
   - aluno/aluna de mestrado
   - aluno/aluna de doutorado
   - pós-doutorando/pós-doutoranda
   - professor/professora
   - pesquisador/pesquisadora (caso nenhuma das anteriores se aplique)
   - prefiro não informar

Obrigado
--------

Agracemos por ter respondido esse questionário.

Os resultados desse questionário serão disponibilizados em https://rgaiacs.gitlab.io/software-na-academia-brasileira/.
