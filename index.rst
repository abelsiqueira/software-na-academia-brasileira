.. Software na Academia Brasileira documentation master file, created by
   sphinx-quickstart on Sun Jun  4 15:02:08 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Resumo
======

Pesquisas como "`UK Research Software Survey 2014 <https://www.software.ac.uk/blog/2014-12-04-its-impossible-conduct-research-without-software-say-7-out-10-uk-researchers>`_"
tem demonstrado a importância de software na pesquisa científica.
Esse projeto objetiva coletar informações sobre o uso de software
por pesquisadores brasileiros.

Sumário
-------

.. toctree::
   :maxdepth: 2

   fase1/index
   CONTRIBUTING
   LICENSE
